/**
 * Created by josempena on 6/03/17.
 */
import React from 'react';
import { Text, View } from 'react-native';

const Header = (props) => {
  const { textStyle, viewStyle } = styles;
  return (
    <View style={viewStyle}>
      <Text style={textStyle}>{props.headerText}</Text>        
    </View>
  );  
};

const styles = {
  viewStyle: {
    // paddingTop must be defined for iOS
    // paddingTop: 15,
    backgroundColor: '#F8F8F8',
    justifyContent: 'center',
    alignItems: 'center',
    height: 60,
    elevation: 2,
    position: 'relative'
    /*
    // Shadow properties for iOS
    shadowColor: '#000',
    shadowOffset: {width: 0, height: 2},
    shadowOpacity: 0.2*/
  },
  textStyle: {
    fontSize: 20
  }
};

export default Header;